

# My Philosophy
Cloned from [Philo Sofer](https://philosofer123.files.wordpress.com/2016/01/my-philosophy-by-philo-sofer.pdf)

* The primary purpose of this document is to advise myself on how to live well
* While I am comfortable with my positions, I am not certain of any of them  

## List of my philosophical positions
* [Atheism](#atheism)
* [Afterlife skepticism](#afterlife-skepticism)

## Atheism
* I define “atheism” as the view that it is highly implausible that the Religious God exists, and that
there is no good reason to believe that any other god exists
  * I define “Religious God” as a [level 4 god](https://youtu.be/DATQKB7656E?t=46m3s) or as the disembodied, omnipotent, omniscient, omnibenevolent
creator of the universe, who wants all humans to believe in him.
* There is a presumption of atheism because theists propose the addition of a major metaphysical
entity (the Abrahamic God) to what is already known to exist (the physical universe). That is, theists
make an extraordinary claim, and extraordinary claims require extraordinary evidence.
* None of the standard arguments for the existence of the Religious God are persuasive, as they are
all subject to damaging objections. 
* There are a number of effective arguments for atheism, briefly summarized as follows:
  * Evidential argument from suffering
    * Attempts to reconcile the existence of the actual amount of suffering with the existence of an omnipotent, omniscient, omnibenevolent creator have all failed
    * Skeptical theism—the assertion that human cognitive limitations prevent one from determining whether any particular instance of suffering is gratuitous—does not significantly reduce the force of the argument from suffering. As it is highly implausible that every single instance of suffering in the world is logically necessary to produce some future compensating good, the argument from suffering continues to show that it is highly implausible that the Abrahamic God exists.
  * Arguments from faulty design
    * The slow evolution of life on earth, involving disastrous trial and error, is evidence against the existence of an omnipotent, omnibenevolent creator
    * The fact that humans require an easily damaged brain is evidence against the existence of a disembodied, omnipotent, omnibenevolent creator: if God has a mind without a brain, why would he not create humans the same way?

    * The design of humans, and nature generally, is wasteful and messy, inefficient and full of needless vulnerabilities and imperfections, pitfalls and limitations. Such flawed design is evidence against an omnipotent, omnibenevolent creator.
* Argument from reasonable nonbelief
  * If God is both omnipotent and wants humans to believe in him, then why has he not made it so obvious that he exists that every reasonable person believes?

## Afterlife skepticism
* Afterlife skepticism is the view that it is highly implausible that there is an afterlife
* I define “afterlife” as consciousness after bodily death, including brain death
* There is no reliable scientific evidence of consciousness after bodily death. At the same time, there is plenty of scientific evidence that all aspects of consciousness (sense impressions, emotions, thoughts, memories, etc.) depend completely upon a live and functioning brain. This indicates that consciousness ends with brain death.
* Consciousness after bodily death appears to require the existence of immaterial souls. But there are effective arguments against the existence of immaterial souls:
  * A soul that houses one’s essence must be able to interact with the body (and particularly with the brain). But how can an immaterial entity interact with a material entity? First, such interaction would appear to be impossible. And second, such interaction would violate the causal closure of the physical, a principle of which no violation has ever been found.
