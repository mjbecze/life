
€
Text before
<blockquote>Blockquote</blockquote>
Text after

<ins>Inserted</ins>

or

<u>Underline</u>
Fixed width text	
<code>Source code</code>

or

<tt>Fixed width text</tt>
<pre>Text is '''preformatted''' and 
''markups'' '''''cannot''''' be done</pre>
<pre style="color: red">Text is '''preformatted''' 
with a style and 
''markups'' '''''cannot''''' be done
</pre>
&copy;
&delta;
&euro;
&amp;euro;  → &euro;
<span style="color: red; text-decoration: line-through;">Typo to be corrected</span>  → Typo to be corrected
&lt;span style="color: red; text-decoration: line-through;">Typo to be corrected</span>  → <span style="color: red; text-decoration: line-through;">Typo to be corrected</span>
