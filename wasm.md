WASM
====

polyfill 
====
EVM code would be transcompiled to WASM. 

1) change push / pop to registers
2) detect static jump condiitons

## Gas Insertion
can be inserted by an addition compulation step

blockchain functions would have to be a [module](https://github.com/WebAssembly/design/blob/9974cded38e762aa6e00098a7d1e6b6f0c270ba0/Web.md)
[Related]( https://github.com/WebAssembly/design/issues/362#issuecomment-142003374)
```
To synchronously call into JavaScript from C++, the C++ code would declare and call an undefined extern function and the target JavaScript function would be given the (mangled) name of the extern and put inside the imported ES6 module.
```
These function inculde
* EVM trap conditions
* CALLS


POSTPOLYFILL
===
Post polyfill would just be the pure WASMVM

* Needs to be fully deterministic https://github.com/WebAssembly/design/blob/master/Nondeterminism.md
  * Disallow Threads
  * Disallow floating point SIMD ops.  values may or may not have subnormals flushed to zero. 
  * Disallow the *Aproximation ops see (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/SIMD/reciprocalApproximation)
  * Callstack space needs to be capped (mem is currently capped at 4gig)
  
 REMOVE call_code  
 REMOVE codeHash    
 code should just be 0 segament in storage    
 code should just run from memory; compilers should keep track of the offset   
 this will do 2 things; give more accurt gas calcuation for memory  
